const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

function verifyToken(request, response, next) {
  const token = request.body.token || request.query.token || request.headers['x-acces-token'];
  if (token) { //
    jwt.verify(token, '9671bf34abf8f17bf2b9a131267c9bf', (err, decoded) => {
      if (err) {
        response.json({
          error: true,
          message: 'Key not found: Access denied ಠ_ಠ',
          objs: {}
        });
      } else {
        next();
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Key not found: Access denied ಠ_ಠ',
      objs: {}
    });
  }
}

module.exports = {
  verifyToken
}
